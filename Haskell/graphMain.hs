module Main where
import WorldviewGraphData
import ToDot
import ToBash
import ToMarkdown ( build )
-- main = putStr (worldviewGraphDataToDotString $ removeAnnotations worldviewGraphData)
-- main = putStr (worldviewGraphDataToDotString worldviewGraphData)
-- main = putStr (worldviewGraphDataToFileList worldviewGraphData)
main = putStrLn $ worldviewGraphDataToDotString worldviewGraphData

-- neuron -d ./Built rib -wS
-- neuron -d ./Built rib

-- From /worldview-explorer/
-- dot -Tsvg -o Markdown_Files/Built/.neuron/output/worldview_tree.svg graph.gv 

-- TODO improve markdown titles for annotations
-- TODO refactor code that calculates the slack channel name as it is being reused to often (search for 80)

-- ghci toSlack.hs
-- putStrLn $ worldviewGraphDataToSlack worldviewGraphData