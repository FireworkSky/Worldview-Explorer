module Main where
import WorldviewGraphData
import ToDot
import ToBash
import ToSlack
import ToMarkdown

main = putStr (worldviewGraphDataToSlack worldviewGraphData)
-- main = putStr (worldviewGraphDataToSlackDesc worldviewGraphData)
