module ToBash where

import WorldviewDatatypes
import WorldviewGraphData

worldviewGraphDataToFileList w = foldl (++) "" (
                                  map (\b -> "touch " ++ beliefId b ++ ".md\n") (beliefs w)
                                  ++ map (\r -> "touch " ++ reasoningStepId r ++ ".md\n") (reasoningSteps w)
                                  ++ map (\a -> "touch " ++ annotationId a ++ ".md\n") (annotations w)
                                  ++ map (\n -> "touch " ++ noteId n ++ ".md\n") (notes w))



f = 1:2:4:[]