module ToDot where

import WorldviewDatatypes
import WorldviewGraphData
import SlackIdData

annotationColour CoherentAnnotation = "green"
annotationColour DissonantAnnotation = "red"

-- >>> s
-- "ab"
s = "a" ++ "b"


egB = Belief {beliefIdPart="blank_slate", beliefPosition = Agree 99, beliefSummary="Blank Slate"}
-- >>> beliefToDotString egB
-- "blank_slate [label=\"Blank Slate\"]"
beliefToDotString b = beliefIdPart b ++ " [label=\"" ++ beliefSummary b ++ "\", style=filled, color=\"" ++ beliefColour b ++ "\", URL=\"https://worldview-explorer.netlify.app/" ++ beliefId b ++ "\"]"

egA = Annotation {annotationType=CoherentAnnotation, annotationIdPart="test", annotationSummary="Test annotation", annotationPointsTo = [ "blank_slate", "test_state"] }
-- >>> annotationToDotString egA
-- "CoherentAnnotation__test -> blank_slate [color=\"green\"]\nCoherentAnnotation__test -> test_state [color=\"green\"]\n"
annotationToDotString a = 
    let id = annotationId a in
    let colour = annotationColour (annotationType a) in
    id ++ "[label=\"" ++ annotationSummary a ++ "\",color=\"" ++ colour ++ "\",fontcolor=\"" ++ colour ++ "\",fontsize=\"8\",URL=\"https://worldview-explorer.netlify.app/" ++ annotationId a ++ "\"]\n" ++
    (foldl (++) "" (map (\toPoint -> id ++ " -> " ++ toPoint ++ " [color=\"" ++ colour ++ "\"]\n") (annotationPointsTo a)))


egR = ReasoningStep {reasoningStepSummary="Reasoning Label", reasoningStepFrom="blank_slate", reasoningStepTo="god_exists"}
-- >>> reasoningStepToDotString egR
-- "blank_slate -> god_exists [label=\"Reasoning Label\", fontsize=\"7\"]"
reasoningStepToDotString :: ReasoningStep -> [Char]
reasoningStepToDotString r = reasoningStepFrom r ++ " -> " ++ reasoningStepTo r ++ " [label=\"" ++ reasoningStepSummary r ++ "\", fontsize=\"7\",URL=\"https://worldview-explorer.netlify.app/" ++ reasoningStepId r ++ "\"]"

a = [[1],[3,4]]

removeAnnotations w = WorldviewGraphData {beliefs = beliefs w, reasoningSteps = reasoningSteps w, annotations = [], notes = notes w}

worldviewGraphDataToDotString w = foldl (++) "" (
                                  ["digraph {\n\n", "graph [bgcolor=black];\nedge [fontcolor=white, color=white];\nnode[fontcolor=black, color=white];\n\n"] ++
                                  map (\b -> beliefToDotString b ++ "\n") (beliefs w) ++ ["\n\n"]
                                  ++ map (\r -> reasoningStepToDotString r ++ "\n") (reasoningSteps w) ++ ["\n\n"]
                                  ++ map (\a -> annotationToDotString a ++ "\n") (annotations w)
                                  ++ ["\n}"]
                                  )
testW = WorldviewGraphData [egB, egB] [egR, egR] [egA, egA]
-- >>> worldviewGraphDataToDotString testW
-- "blank_slate [label=\"Blank Slate\"]\nblank_slate [label=\"Blank Slate\"]\n\nblank_slate -> god_exists [label=\"Reasoning Label\", fontsize=\"7\"]\nblank_slate -> god_exists [label=\"Reasoning Label\", fontsize=\"7\"]\n\nCoherentAnnotation__test -> blank_slate [color=\"green\"]\nCoherentAnnotation__test -> test_state [color=\"green\"]\n\nCoherentAnnotation__test -> blank_slate [color=\"green\"]\nCoherentAnnotation__test -> test_state [color=\"green\"]\n\n"



f = 1:2:4:[]