module ToMarkdown where

import System.Environment
import WorldviewDatatypes
import WorldviewGraphData
import SlackIdData
import Control.Exception

data CustomException = MissingSlackId String
    deriving Show

instance Exception CustomException


outgoingSnippetBelief b w =  let leadsToList = (outgoingReasoningSteps b w) in if length leadsToList == 0 then "\n\nLeads to:\n - _Nowhere_ (yet)" else foldl (++) "\nLeads to:\n" (map (\r -> " - [[[" ++ reasoningStepId r ++ "]]]\n") leadsToList)

outgoingSnippetReasoningStep r w = "\n\nLeads to: \n - [[[" ++ (beliefId (beliefFromIdPart (reasoningStepTo r) w)) ++ "]]]"
-- Above relies on reasoning steps only leading to beliefs. TODO fix LOW priority

outgoingSnippetAnnotation a w = let leadsToList = (annotationPointsTo a) in if length leadsToList == 0 then "" else foldl (++) "\n\nRefers to:\n" (map (\x -> " - [[" ++ (beliefId (beliefFromIdPart x w)) ++ "]]\n") leadsToList)
-- Above will not work if annotations point to edges. TODO fix MEDIUM priority

outgoingSnippetNote n w = ""

listAllWithTitlesAndOutgoingSnippetAndSlackChannelName w = map (\b -> (beliefId b, beliefSummary b, outgoingSnippetBelief b w, beliefIdLimitedLength 80 b)) (beliefs w)
                                  ++ map (\r -> (reasoningStepId r, beliefSummary (beliefFromIdPart (reasoningStepFrom r) w) ++ " to " ++ beliefSummary (beliefFromIdPart (reasoningStepTo r) w), outgoingSnippetReasoningStep r w, reasoningStepIdLimitedLength 80 r)) (reasoningSteps w)
                                  ++ map (\a -> (annotationId a, annotationId a, outgoingSnippetAnnotation a w, annotationIdLimitedLength 80 a)) (annotations w)
                                  ++ map (\n -> (noteId n, noteSummary n, outgoingSnippetNote n w, noteIdLimitedLength 80 n)) (notes w)



endSnippet x outgoingSnippet slackChannelName = if (slackIdData slackChannelName) == "--MISSING-SLACK-ID--" then (throw (MissingSlackId x)) else "\n\n----\n\n[Join the conversation on Slack](https://worldview-explorer.slack.com/archives/" ++ (slackIdData slackChannelName) ++ ")\n\n[Join the Slack workspace here if you haven't already](https://join.slack.com/t/worldview-explorer/shared_invite/zt-gk0pw09g-ypHCfVQRlegdTN95YY3QPQ)\n" ++ outgoingSnippet
startSnippet title = "# " ++ title ++ "\n\n"

-- main = sequence $ map (\(x,y) -> writeFile
--   ("Out/" ++ x ++ ".md") 
--   ( "# " ++ y ++ "\n\n" ))
--   (zip (listAll worldviewGraphData) (listAllSummaries worldviewGraphData))
  
build = sequence $ map (\(x, title, outgoingSnippet, slackChannelName) ->
  ( do indexContents <- readFile ("index.md") -- Copy index.md contents to Built folder as they are
       writeFile "Built/index.md" indexContents
       writtenContents <- readFile (x ++ ".md")
       writeFile ("Built/" ++ x ++ ".md") ((startSnippet title) ++ writtenContents ++ (endSnippet x outgoingSnippet slackChannelName))
       )
       )
  (listAllWithTitlesAndOutgoingSnippetAndSlackChannelName worldviewGraphData)