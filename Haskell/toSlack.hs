module ToSlack where

import WorldviewDatatypes
import WorldviewGraphData
import SlackIdData


createChannelPrefix = "curl -X POST -H \"Authorization: Bearer $APITOKEN\" \\\n-H 'Content-type: application/json' \\\n--data '{\"name\":\""

createChannelPostfix =  "\"}' \\\nhttps://slack.com/api/conversations.create\n\n"

listAllSlackIds w = map (\b -> beliefIdLimitedLength 80 b) (beliefs w)
                                  ++ map (\r -> reasoningStepIdLimitedLength 80 r) (reasoningSteps w)
                                  ++ map (\a -> annotationIdLimitedLength 80 a) (annotations w)
                                  ++ map (\n -> noteIdLimitedLength 80 n) (notes w)

uncreatedSlackIds w = filter (\x -> slackIdData x == "--MISSING-SLACK-ID--") (listAllSlackIds w)

worldviewGraphDataToSlack w = foldl (++) "" (map (\x -> createChannelPrefix ++ x ++ createChannelPostfix) (uncreatedSlackIds w))

worldviewGraphDataToSlackDesc w = foldl (++) "" (map (\(x, y) -> let desc = "https://worldview-explorer.netlify.app/" ++ x in setDescCommand (slackIdData y) desc) (zip (listAll w) (uncreatedSlackIds w)))  

setDescCommand channel desc = "\ncurl -X POST -H 'Authorization: Bearer '\"$APITOKEN\"'' \\\n-H 'Content-type: application/json' \\\n--data '{\"channel\":\"" ++ channel ++ "\", \"purpose\":\"" ++ desc ++ "\"}' \\\nhttps://slack.com/api/conversations.setPurpose\n"

-- To set description for channel:
-- curl -X POST -H 'Authorization: Bearer '"$APITOKEN"'' \
-- -H 'Content-type: application/json' \
-- --data '{"channel":"C019QU88KT4", "purpose":"Testing123 \nhttps://google.com"}' \         
-- https://slack.com/api/conversations.setPurpose

-- To set topic for channel:
-- curl -X POST -H 'Authorization: Bearer '"$APITOKEN"'' \
-- -H 'Content-type: application/json' \
-- --data '{"channel":"C019QU88KT4", "topic":"Testing123"}' \
-- https://slack.com/api/conversations.setTopic