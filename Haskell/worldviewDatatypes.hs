module WorldviewDatatypes where

import Data.Hashable ( Hashable(hash) )
import Data.Colour
import Data.Colour.SRGB
import Data.Colour.RGBSpace
import Data.Colour.CIE
import qualified Data.Colour.CIE.Illuminant
import GHC.Float

type IdString = String -- Would be nice to restrict this to lowercase plus underscores only

data Position =
      Agree Int
    | Disagree Int
    | Unsure Int deriving (Show)

data Belief = Belief {
    beliefIdPart :: IdString
    , beliefSummary :: String
    , beliefPosition :: Position } deriving (Show)
    
data ReasoningStep = ReasoningStep {
    reasoningStepSummary :: String
    , reasoningStepFrom :: IdString
    , reasoningStepTo :: IdString } deriving (Show)

data AnnotationType = 
    CoherentAnnotation
    | DissonantAnnotation

instance Show AnnotationType where
    show CoherentAnnotation = "coherent"
    show DissonantAnnotation = "dissonant"

data Annotation = Annotation {
    annotationType :: AnnotationType
    , annotationIdPart :: IdString
    , annotationSummary :: String
    , annotationPointsTo :: [ IdString ] } deriving (Show)

data Note = Note {
    noteIdPart :: IdString
    , noteSummary :: String } deriving (Show)

data WorldviewGraphData = WorldviewGraphData {
    beliefs :: [ Belief ]
    , reasoningSteps :: [ ReasoningStep ]
    , annotations :: [ Annotation ] 
    , notes :: [ Note ]
} deriving (Show)


myGamut =  mkRGBGamut (RGB (mkChromaticity 0.63 0.34)
                          (mkChromaticity 0.31 0.595)
                          (mkChromaticity 0.155 0.07))
                     Data.Colour.CIE.Illuminant.d65

myTransferFunction = TransferFunction id id 1
rgbSpace = mkRGBSpace myGamut myTransferFunction

hexOfRGB r g b = sRGB24show (rgbUsingSpace rgbSpace r g b)

hexColourOfPosition (Agree n) = hexOfRGB 0 ((int2Double n)/75 + 0.25) 0
hexColourOfPosition (Disagree n) = hexOfRGB ((int2Double n)/75 + 0.25) 0 0
hexColourOfPosition (Unsure n) = hexOfRGB 0 0 ((int2Double n)/50 + 0.50)

beliefColour b = hexColourOfPosition (beliefPosition b)

shortHash s = show (hash s `mod` 10000)

beliefId b = beliefIdPart b ++ "__belief"
annotationId a = show (annotationType a) ++ "__" ++ annotationIdPart a ++ "__annotation"
reasoningStepId r = reasoningStepFrom r ++ "__to__" ++ reasoningStepTo r ++ "__reasoning_step"

noteId n = noteIdPart n ++ "__note"

beliefIdLimitedLength l b = let bHash = shortHash (beliefId b) in if length (beliefId b) > l then (take (l - length ("-etc__belief-" ++ bHash)) (beliefId b)) ++ "-etc__belief-" ++ bHash else beliefId b
annotationIdLimitedLength l a = let aHash = shortHash (annotationId a) in if length (annotationId a) > l then (take (l - length ("-etc__annotation-" ++ aHash)) (annotationId a)) ++ "-etc__annotation-" ++ aHash else annotationId a

reasoningStepPartLimitedLength l s = if length s > l then take l s ++ "-etc" else s

reasoningStepIdLimitedLength l r = let rHash = shortHash (reasoningStepId r) in let maxLengthPerLinkedBelief = (quot (l - length ("__reasoning_step-" ++ rHash ++ "__to__")) 2) - length "-etc" in if length (reasoningStepId r) > l then 
    reasoningStepPartLimitedLength maxLengthPerLinkedBelief (reasoningStepFrom r) ++ "__to__" ++ reasoningStepPartLimitedLength maxLengthPerLinkedBelief (reasoningStepTo r) ++ "__reasoning_step-" ++ rHash else reasoningStepId r

noteIdLimitedLength l n = let nHash = shortHash (noteId n) in if length (noteId n) > l then (take (l - length ("-etc__note-" ++ nHash)) (noteId n)) ++ "-etc__note-" ++ nHash else noteId n

listAll w = map (\b -> beliefId b) (beliefs w)
                                  ++ map (\r -> reasoningStepId r) (reasoningSteps w)
                                  ++ map (\a -> annotationId a) (annotations w)
                                  ++ map (\n -> noteId n) (notes w)

outgoingReasoningSteps b w = filter (\r -> reasoningStepFrom r == beliefIdPart b) (reasoningSteps w)

beliefFromIdPart x w = head (filter (\b -> beliefIdPart b == x) (beliefs w))