module WorldviewGraphData where

import WorldviewDatatypes

worldviewGraphData =
  WorldviewGraphData
    { beliefs = [
        Belief {beliefIdPart = "blank_slate", beliefPosition = Agree 99, beliefSummary = "Blank Slate"},
        Belief {beliefIdPart = "god_exists", beliefPosition = Agree 95, beliefSummary = "God exists"},
        Belief {beliefIdPart = "god_is_loving", beliefPosition = Agree 94, beliefSummary = "God is loving"},
        Belief {beliefIdPart = "christian_god_exists", beliefPosition = Agree 82, beliefSummary = "The Christian God exists"},
        Belief {beliefIdPart = "no_god_exists", beliefPosition = Disagree 90, beliefSummary = "No God exists"},
        Belief {beliefIdPart = "free_will_is_real", beliefPosition = Agree 92, beliefSummary = "Free will is real"},
        Belief {beliefIdPart = "the_external_world_exists", beliefPosition = Agree 90, beliefSummary = "The external world we personally experience exists"},
        Belief {beliefIdPart = "the_world_today_we_hear_about_via_others_exists", beliefPosition = Agree 82, beliefSummary = "The world today we hear about via others exists"},
        Belief {beliefIdPart = "the_past_world_we_hear_about_existed", beliefPosition = Agree 85, beliefSummary = "The past world we hear about existed"},
        Belief {beliefIdPart = "the_bible_is_historically_reliable", beliefPosition = Agree 55, beliefSummary = "The Bible is historically reliable"},
        Belief {beliefIdPart = "the_bible_is_inerrant", beliefPosition = Unsure 10, beliefSummary = "The Bible is inerrant"},
        Belief {beliefIdPart = "the_bible_is_innerant_but_context_is_critical", beliefPosition = Unsure 5, beliefSummary = "The Bible is inerrant but context is critical"},
        Belief {beliefIdPart = "the_bible_is_innerant_and_should_be_interpreted_literally", beliefPosition = Disagree 65, beliefSummary = "The Bible is innerrant but should be interpreted literally"},
        Belief {beliefIdPart = "the_bible_is_useful", beliefPosition = Agree 72, beliefSummary = "The Bible is useful"},
        Belief {beliefIdPart = "the_bible_is_a_historical_document", beliefPosition = Agree 60, beliefSummary = "The Bible is a historical document"},
        Belief {beliefIdPart = "the_bible_is_a_trustworthy_source_of_jesus_public_teachings", beliefPosition = Agree 68, beliefSummary = "The Bible is a trustworthy source of Jesus' public teachings"},
        Belief {beliefIdPart = "the_bible_is_useful_more_than_a_regular_book_partly_miraculously", beliefPosition = Agree 62, beliefSummary = "The Bible is useful more than a regular book, partly miraculously"},
        Belief {beliefIdPart = "the_bible_is_a_trustworthy_source_of_all_jesus_teachings", beliefPosition = Agree 30, beliefSummary = "The Bible is a trustworthy source of all Jesus' teachings"},
        Belief {beliefIdPart = "the_bible_is_a_trustworthy_source_of_all_jesus_teachings_and_a_bit_more", beliefPosition = Agree 25, beliefSummary = "The Bible is a trustworthy source of all Jesus' teachings and a bit more"},
        Belief {beliefIdPart = "the_bible_is_gods_word", beliefPosition = Unsure 8, beliefSummary = "The Bible is God's word"} -- \n(Note: hard to define) TODO add support for additional notes like this
    ],
      reasoningSteps = [
          ReasoningStep {reasoningStepSummary = "The Moral argument\nThe argument from free will", reasoningStepFrom = "blank_slate", reasoningStepTo = "god_exists"},
          ReasoningStep {reasoningStepSummary = "Jesus - Lord Liar Lunatic\nReliability of Biblical testimony", reasoningStepFrom = "god_exists", reasoningStepTo = "christian_god_exists"},
          ReasoningStep {reasoningStepSummary = "Jesus referred to Old Testament writings as if they were God's Word\nPaul referred to other New Testament writings as God's Word\nThe early church was well placed to make such a judgement having members who had heard Jesus first-hand\nThe early church had the opportunity to denounce newly written Scripture but did not", reasoningStepFrom = "christian_god_exists", reasoningStepTo = "the_bible_is_gods_word"},
          ReasoningStep {reasoningStepSummary = "This emerged as doctrine in the earlyish stages of the church", reasoningStepFrom = "christian_god_exists", reasoningStepTo = "the_bible_is_inerrant"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "the_bible_is_inerrant", reasoningStepTo = "the_bible_is_innerant_and_should_be_interpreted_literally"},
          ReasoningStep {reasoningStepSummary = "The Bible claims to be useful\nThe early church would have been well-placed to refute this claim had it been false", reasoningStepFrom = "the_bible_is_a_historical_document", reasoningStepTo = "the_bible_is_useful"},
          ReasoningStep {reasoningStepSummary = "The argument from evil", reasoningStepFrom = "blank_slate", reasoningStepTo = "no_god_exists"},
          ReasoningStep {reasoningStepSummary = "The argument from personal experience", reasoningStepFrom = "blank_slate", reasoningStepTo = "free_will_is_real"},
          ReasoningStep {reasoningStepSummary = "An ultimate being would be loving", reasoningStepFrom = "god_exists", reasoningStepTo = "god_is_loving"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "blank_slate", reasoningStepTo = "the_external_world_exists"},
          ReasoningStep {reasoningStepSummary = "A level of trust in others today", reasoningStepFrom = "the_external_world_exists", reasoningStepTo = "the_world_today_we_hear_about_via_others_exists"},
          ReasoningStep {reasoningStepSummary = "A level of trust in people in the past\nA level of trust in historical methods", reasoningStepFrom = "the_external_world_exists", reasoningStepTo = "the_past_world_we_hear_about_existed"},
          ReasoningStep {reasoningStepSummary = "A basic level of trust in historical methods\nThe existence of old Biblical manuscripts", reasoningStepFrom = "the_past_world_we_hear_about_existed", reasoningStepTo = "the_bible_is_a_historical_document"}, 
          ReasoningStep {reasoningStepSummary = "A level of trust in historical analysis pertinent to the Bible\nThe vast numbers of original Biblical manuscripts\nThe instances of archaeology verifying Biblical information", reasoningStepFrom = "the_bible_is_a_historical_document", reasoningStepTo = "the_bible_is_historically_reliable"},
          ReasoningStep {reasoningStepSummary = "The public who had listened to Jesus' teachings were well placed to correct any misrepresentations\nThere is evidence the Bible's content has been transmitted reliably over time", reasoningStepFrom = "the_bible_is_a_historical_document", reasoningStepTo = "the_bible_is_a_trustworthy_source_of_jesus_public_teachings"},
          ReasoningStep {reasoningStepSummary = "Evidence of apostilic authorship\nTrust and evidence that the apostles were intending to faithfully represent Jesus' private teachings\nTrust and evidence that the apostles were capable of faithfully representing Jesus' teachings\nPossibly supernatural help for the apostles recalling Jesus' words", reasoningStepFrom = "the_bible_is_a_trustworthy_source_of_jesus_public_teachings", reasoningStepTo = "the_bible_is_a_trustworthy_source_of_all_jesus_teachings"},
          ReasoningStep {reasoningStepSummary = "Evidence and trust that the NT authors intended to write about events trustworthily and were capable\nThe NT documents were transmitted reliably", reasoningStepFrom = "the_bible_is_a_trustworthy_source_of_all_jesus_teachings", reasoningStepTo = "the_bible_is_a_trustworthy_source_of_all_jesus_teachings_and_a_bit_more"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "the_bible_is_historically_reliable", reasoningStepTo = "the_bible_is_inerrant"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "the_bible_is_a_trustworthy_source_of_all_jesus_teachings_and_a_bit_more", reasoningStepTo = "the_bible_is_inerrant"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "the_bible_is_useful", reasoningStepTo = "the_bible_is_useful_more_than_a_regular_book_partly_miraculously"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "christian_god_exists", reasoningStepTo = "the_bible_is_useful_more_than_a_regular_book_partly_miraculously"},
          ReasoningStep {reasoningStepSummary = "", reasoningStepFrom = "the_bible_is_inerrant", reasoningStepTo = "the_bible_is_innerant_but_context_is_critical"}
      ],
      annotations = [
          Annotation {annotationType = CoherentAnnotation, annotationIdPart = "free_will_coheres_with_god_existing", annotationSummary = "God can create free willed agents", annotationPointsTo = ["free_will_is_real", "god_exists"]},
          Annotation {annotationType = CoherentAnnotation, annotationIdPart = "god_being_loving_coheres_with_the_christian_god_existing", annotationSummary = "The Christian God is loving", annotationPointsTo = ["god_is_loving", "christian_god_exists"]},
          Annotation {annotationType = CoherentAnnotation, annotationIdPart = "a_high_view_of_the_bible_coheres", annotationSummary = "A high view of the Bible coheres", annotationPointsTo = [
            "the_bible_is_historically_reliable", "the_bible_is_gods_word",
            "the_bible_is_inerrant", "the_bible_is_useful",
            "the_bible_is_a_trustworthy_source_of_jesus_public_teachings",
            "the_bible_is_a_trustworthy_source_of_all_jesus_teachings",
            "the_bible_is_useful_more_than_a_regular_book_partly_miraculously",
            "the_bible_is_a_trustworthy_source_of_all_jesus_teachings_and_a_bit_more"]},
          Annotation {annotationType = DissonantAnnotation, annotationIdPart = "god_cannot_exist_and_not_exist", annotationSummary = "Obviously", annotationPointsTo = ["god_exists", "no_god_exists"]}
      ],
      notes = [
        Note {noteIdPart = "apostles_creed", noteSummary = "Apostles' Creed"},
        Note {noteIdPart = "free_will_definition", noteSummary = "Definition of free will"},
        Note {noteIdPart = "reply_to_argument_from_evil", noteSummary = "Reply to the argument from evil"}

      ]
    }


