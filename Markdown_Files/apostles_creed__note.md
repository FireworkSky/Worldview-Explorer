I believe in God, the Father almighty,

creator of heaven and earth.

I believe in Jesus Christ, his only Son, our Lord.

He was conceived by the power of the Holy Spirit

and born of the virgin Mary.

He suffered under Pontius Pilate,

was crucified, died, and was buried.

He descended to the dead.

On the third day he rose again.

He ascended into heaven,

and is seated at the right hand of the Father.

He will come again to judge the living and the dead.

I believe in the Holy Spirit,

the holy catholic Church,  *— Note from me: Here I would generalise to a wider range of churches than what we now call the Catholic church, the creed was written well before the protestant reformation!*

the communion of the saints,

the forgiveness of sins,

the resurrection of the body,

and the life everlasting. Amen.

---

See the International Consultation on English Texts version in [Apostles' Creed - Wikipedia](https://www.notion.so/Apostles-Creed-Wikipedia-5626798a4c9b4f11bbc3db5f5fadcc71) for the source of the above creed. The exact version of the creed does not matter too much to me, the versions I am familiar with are similar enough to me. One interesting wording change in the English Language Liturgical Consultation version is switching out referring to God as "he"/"his" by just saying "God" - I think this is helpful, but not critical for two reasons. Firstly that God is referred to as "he"/"his" throughout the Bible and so I'm not sure it changes much. Secondly, and more importantly, I think that even if we refer to God as "he" it is reasonable to do so while maintaining that God is a different kind of being to humans and so can be genderless, even though for simplicity's sake we refer to him that way.

All that to say, I don't think the minor differences between the apostles' creed versions amount to that much, which helps me say that I think I do actually believe something quite similar to those who also held to the creed from over a thousand years ago.