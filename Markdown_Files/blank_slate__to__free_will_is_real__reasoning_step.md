One reason to move from a blank slate to the belief that free will is real is the argument from personal experience.

## The argument from personal experience
As I write this, I know from my own personal experience that it seems undeniable that I have a very strong sense of having the ability to make choices for myself. Naturally, the question is whether these choices are actually my own (resulting from me exercising free will) or whether they are somehow not real. What is undeniable though is that they _feel_ real to me. I am assuming that any person reading this has a similar experience.

This feeling of exerting free will is so close to the core of my being that it feels like one of the things I am most sure about. For example, I experience exerting free will choices often with such a clear experience that I would have no hesitation in agreeing that I can or should be held accountable for my own choices. So, either there is some gigantic conspiracy designed to trick me very deeply in regards to such a basic experience or they are in fact real free will choices.

It seems to me that I this feeling of being able to make free will choices is a more basic belief than almost any other belief I could hold. For example, this feeling of having free will is stronger than me even knowing what my own name is, who my parents are, that the external world is real, and so on. For example, even if I were plugged into a Matrix (akin to the one in the classic movie), even then I would _still_ experience free will and so this is an extremely fundamental belief.

I think the strongest standalone argument for whether you have free will or not is to personally examine your own mind. I really doubt that believing anything other than that this sense of free will being is real is a plausible position without positing an extremely malicious god-like being creating the illusion of free will - which to me seems like something I can disregard as very unlikely.

Here is how I like to define free will: [[free_will_definition__note]]