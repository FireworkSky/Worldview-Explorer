The move from a blank slate to that God exists is a a critical step that the discussion here cannot hope to fully unpack. Nevertheless, some reasons that may contribute to this step include: the moral argument and the argument from free will.

## The argument from free will

This belief coheres with that free will exists because of reasons outlined in [[coherent__free_will_coheres_with_god_existing__annotation]]. But it's also important to see that the existence of free will gives us reason to believe that there is a God. If there is no God, it seems impossible for free will to exist (and not some illusory experience of it). Any set of natural laws alone, no matter how complex, could never result in a free-willed being because the being is controlled by the natural laws alone and as such cannot be freely making decisions. (See [[free_will_definition__note]]).

It's worth noting that this is not a God of the gaps argument. Rather, by (my) definition of free will, and the hypothesis that the beings are controlled by natural laws alone it is evident (at least to me) that no free-willed being could exist that is controlled solely by the natural laws without drastically changing what one means by "natural laws" to the extent of giving God-like properties to 'natural' laws. Feel free to disagree with me in the Slack channel.

## The moral argument

Until I write something substantive here, here's the link to the [Stanford Encyclopedia of Philosophy on Moral Arguments for the Existence of God](https://plato.stanford.edu/entries/moral-arguments-god/). (Disclaimer, I haven't read much of it yet!) <!--TODO read SEP article and improve this section-->