There are a number of possible reasons why one might move from a black slate to believing that _no_ God exists. Personally, I don't think the reasons for taking this step are justified but feel free to discuss your reasons in the Slack channel.

## The Argument from Evil
One relatively common reason is the argument from evil. It boils down to the existence of evil being dissonant with the existence of God. Some would argue that the existence of evil _proves_ that a loving, all-powerful God cannot exist because surely such a God would eradicate evil or make it never have been a possibility (created a 'better' world/universe).

I'll leave my thoughts on this stance for the [[reply_to_argument_from_evil__note]].