That the external world exists is critical for many forms of reasoning relevant to our belief system, such as any historical analysis.

Our personal experience is a strong reason why (almost) all of us believe and act as though the external world is real. By going about our day to day business, we generally assume that the world and the pressures it places upon us are real and that it's worth acting as if they are.

But, usually, we don't just act as if they are real but we really do believe the external world is real. It tends to only be in philosophical trains of thoughts that we consider it not being real.

In saying all that, I think it's fair to admit that it is a leap of faith to assume the external world does indeed exist. However it's a relatively small leap that almost everyone makes daily.

So, I think it's fair to assume the external world exists and that it is reasonable to expect others do too. We can then reason about our personal experience and history more easily based upon what really _is_ common ground between us. But considering 'the Matrix' does have it's place too 🔵🔴.