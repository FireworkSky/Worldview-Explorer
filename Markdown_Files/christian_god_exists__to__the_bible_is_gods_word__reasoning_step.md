Some reasons someone who believes that the Christian God exists might move to believing that the Bible is God's Word are the following: (It is difficult to define exactly what is meant by the Bible being God's word, see [[the_bible_is_gods_word__belief]] for more)

## Jesus referred to Old Testament writings as if they were God's Word
The way Jesus is described to refer to Old Testament Scripture is as if he believed it to be true and to carry much of the weight we would associate with 'the Word of God'. This page [here]() has some examples of where Jesus referred to the old testament
## Paul referred to other New Testament writings as God's Word

In 2 Peter 3:14-16 Peter referred to some of Paul's writings as Scripture, in particular he was referring to Paul's writings and then said "... as they do _the other Scriptures_ ..." (emphasis mine). This gives some indication that Peter considered Paul's writings to be Scripture.

Here are those verses:

> 14 So then, dear friends, since you are looking forward to this, make every effort to be found spotless, blameless and at peace with him. 15 Bear in mind that our Lord’s patience means salvation, just as our dear brother Paul also wrote you with the wisdom that God gave him. 16 He writes the same way in all his letters, speaking in them of these matters. His letters contain some things that are hard to understand, which ignorant and unstable people distort, as they do the other Scriptures, to their own destruction. (NIV)

It seems reasonable to assume that by referring to Paul's writing as Scripture Peter believed they were "God's Word". Though as I've said before it's difficult to say exactly what is meant by "God's Word" and to know exactly what Peter meant. But there is some indication of the belief of some of Paul's writings being God's Word as a result of what Peter said.

## The early church was well placed to make such a judgement having members who had heard Jesus first-hand

It seems reasonable to assume that the early church firstly would have heard the words of Jesus preached first-hand and even more would have heard second-hand from people they trusted about what Jesus preached. Because of this it seems reasonable to assume that much of the early church would have known what Jesus preached and as such would have been in a position to judge and reject writings that went against what Jesus himself preached.

Due to this it seems reasonable that the early church had the opportunity to denounce what we now call the New Testament Scripture and would have had the first or second-hand knowledge necessary in order to make good decisions about it. As such, the way in which the Scripture we now have survived and indeed thrived indicates that it was indeed a good reflection of Jesus' teachings.

## The early church had the opportunity to denounce newly written Scripture but did not

That the early church was well placed to make a judgement about Scripture is helpful but it would cease to be helpful if the way the early church worked was such that despite this knowledge there was not the opportunity to denounce writings considered not to reflect Jesus' teachings. Here I have to consider the general fact that in most circumstances the early church might have been in it would be reasonable to assume that its members would have the opportunity to voice their concerns and address false writings. Why wouldn't they have this opportunity? Perhaps if there was tyranny, an authoritarian and harsh regime in place or some other threat that would hold back people voicing any concerns honestly. However, it seems unlikely that this kind of situation was anything like widespread enough amongst the early church to truly hold back the voice of any who were concerned. So it seems reasonable to assume that they would have had the opportunity. _But any historians who have information I could add to this point (or that dispute it) please connect via the Slack link at the bottom of the page :)_

As the early church would have been well placed to make a judgement and would likely have had the opportunity it seems reasonable to suggest that the Scriptures that survived and thrived are in fact representative of Jesus' teachings.


