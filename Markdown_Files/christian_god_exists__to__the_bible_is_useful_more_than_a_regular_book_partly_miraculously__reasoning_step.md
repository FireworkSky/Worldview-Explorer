If the Christian God exists then it seems reasonable that the Bible, being a Christian document that claims in part to have had some miraculous influence involved in its writing, would be more useful than a regular book. This seems sufficiently clear to me.

Here's a couple of claims from the Bible that support the idea of some miraculous events being involved in its writing. Note that these are relatively vague and in some ways minor, the Bible doesn't claim drastic miraculous involvement in the writing of itself.

2 Timothy 3:16
> All Scripture is God-breathed ... (NIV)

John 14:26
> But the Advocate, the Holy Spirit, whom the Father will send in my name, will teach you all things and will remind you of everything I have said to you. (NIV)