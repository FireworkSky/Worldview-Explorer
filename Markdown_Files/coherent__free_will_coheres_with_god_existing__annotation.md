God serves as a plausible source for the creation of free willed beings. God is typically considered to be _very_ powerful (at least) and as such it seems reasonable that God would know how to create free willed beings.

On the other hand, the existence of (true) free willed beings points to some entity powerful enough to create such free-willed beings, which points towards the existence of God.