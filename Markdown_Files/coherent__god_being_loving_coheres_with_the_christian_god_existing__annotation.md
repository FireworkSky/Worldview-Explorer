The Christian God is strongly associated with the notion of Love. For example, the phrase "God is Love" is common (see [1 John 4:16](https://www.biblegateway.com/passage/?search=1%20John%204%3A16&version=NIV)).

In Matthew 22:36-40 Jesus teaches that the greatest command is to love God and one another:

>  36 “Teacher, which is the greatest commandment in the Law?”

> 37 Jesus replied: "'Love the Lord your God with all your heart and with all your soul and with all your mind.' 38 This is the first and greatest commandment. 39 And the second is like it: 'Love your neighbor as yourself.' 40 All the Law and the Prophets hang on these two commandments." _(NIV)_

Because of this it is clear that a loving God existing coheres with the Christian God existing.