The definition of free will I like is:

> The ability to freely choose between at least two options, even just between what to think about.

Freely means:

> Not constrained or forced into the choice by the environment, _including_ not being constrained by the deterministic processing of random information sources.