Here is how I like to define free will: [[free_will_definition__note]]

This belief includes the notion that free will is _really_ real. That humans (at least) have the ability to make choices that are not derived from anything in their environment nor from random sources but actually belong to 'them'. An important consequence of this is that it allows for the possibility of humans (at least) being able to be _held to account_ for their choices, in particular for moral choices.

Specifically this belief is _not_ that humans have the experience of free will but don't _actually_ have it.