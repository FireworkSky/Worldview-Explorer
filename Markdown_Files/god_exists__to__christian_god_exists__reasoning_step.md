This is a tough, challenging step to summarise in a few paragraphs but here goes:


_Given some God exists, why the Christian God?_


## Jesus - Lord Liar Lunatic
Probably the key reason I believe in the Christian God as opposed to any other God is the evidence around Jesus and the events of his life and related events.

One part of this is the Lord, Liar, Lunatic argument attributed to C.S. Lewis (a classic argument in Christian circles).

The gist of it goes like this:

Based on the evidence we can be sufficiently sure that:
- Jesus claimed to be God
- Jesus was crucified
- Jesus could have avoided being crucified by backing down on his claim of being God 

So, one of the following must be true:
1. Jesus was God as he claimed
2. Jesus was lying
3. Jesus was a lunatic (mistakenly believing he was God)

First considering 2.
### Was Jesus a liar
From what we know about Jesus we are told he was a righteous and upright person whose moral teachings were outstanding and _good_. We even get this idea from extra-biblical sources such as Josephus.

From [the Arabic version](https://pages.uncc.edu/james-tabor/ancient-judaism/josephus-jesus/) (that is less likely to have been tampered to support Christianity - though I guess it could have been altered to supply less support to Christianity than beforehand - in any case Josephus probably _at least_ said this):

> "... there was a wise man who was called Jesus, and his conduct was good, and he was known to be virtuous ..."

Given the fact that Jesus was considered good and virtuous to the extent that even a historian would record such things about him and that many of his teachings continue to have a positive impact via Christianity today, it seems _really unlikely_ that he would be _deliberately_ misleading his followers and all future followers by knowingly claiming to be God. Thus we can be sufficiently sure that he was not a liar (at least about whether he was or wasn't God - I don't claim this evidence _alone_ proves he never told a lie ever).

### Was Jesus a lunatic
Based upon similar evidence, it seems that we can be sufficiently sure that the people in Jesus' time didn't consider him to be a lunatic - his teaching was considered wise.

> "there was a **wise** man who was called Jesus" (emphasis mine)

While possible, it is unlikely that such a highly regarded figure (especially when one takes the gospels of the Bible into account) would have mistakenly thought he was God - especially in a culture such as the Jewish culture where there was a clear(ish) understanding of who God was.

As such, it seems unlikely that Jesus was a lunatic, though admittedly based on the evidence here alone it is possible. So it's worth considering adding into the mix some evidence of miraculous events surrounding Jesus' life such as Josephus reports:

> "They [his disciples] reported that he had appeared to them three days after his crucifixion, and that he was alive."

Also there are various reports via the Bible and from the early Church that indicate that Jesus performed various miracles during his lifetime which further validate his claim to be the Messiah/God, along with fulfilled prophecy. These will be discussed elsewhere. <!--TODO add link to where Jesus related miracles are discussed --> But it's worth mentioning them here a little as they add a note of authenticity to Jesus claim to be the Messiah/God that otherwise would leave us more skeptical (thinking that perhaps he was a lunatic) than if they had not occurred.

Overall this leaves us sufficiently certain that Jesus wasn't a lunatic.

### Jesus was God
This leaves us with the only remaining option (or does it?). That Jesus was, as he claimed, God.

What are the other options if this is still something we refuse to believe? Well, we can reject some of the evidence which shifts the investigation towards the validity of the evidence. Or we can offer other options other than liar or lunatic. One such option that has been suggested is "legend" where Jesus' disciples exaggerated the stories of Jesus to the extent that the became "God" only in legend and that he never claimed that himself. To be precise this is simply casting doubt on the evidence that he claimed to be God but in any case it's worth considering.

### Was Jesus a legend?
To me, this becomes a bit of a meta-level argument because I think it shifts the question of Lord Liar Lunatic onto the disciples. The disciples were also under a lot of pressure similar to crucifixion with many of them becoming martyred when killed for the Christian beliefs. So, did the disciples _knowingly_ exaggerated Jesus to say he claimed to be God (lying), did they _unknowingly_ do so (lunatics) - this seems really unlikely because how would they all get caught up in the same delusion with sufficient teamwork to pull off a deception..., or, did Jesus actually claim to be Lord as we asserted in the original evidence?

It seems unlikely that the disciples knowingly exaggerated the claims of Jesus because they themselves were under persecution and so there would really have been no motive for them to knowingly hold on to and publicise false beliefs - what would have been their motive? To all together be persecuted more for a belief they would (in this branch of reasoning) have _known_ to be true? To me it seems sufficiently clear that this would not have happened.

There are further historical arguments that come into play here, such as how recent after the events were the reports of what happened to Jesus that we are somewhat relying upon (the gospels, letters and Josephus). Within a reasonable margin of error I don't think the answers to such questions impact the overall conclusions here though - but feel free to disagree in the Slack channel. Perhaps in the future I will delve more into the evidence myself and write more here.

But overall, I think we can conclude that Jesus wasn't a legend, with sufficient certainty.

So the question shifts from was Jesus God to can we trust the evidence around Jesus - that's the Lord Liar Lunatic argument (roughly). Is it convincing? Please feel free to leave your thoughts in the Slack channel (link at the bottom).

## Reliability of Biblical testimony

The Bible itself makes a clear claim that Jesus is God. For example John 10:24-30,
> "The Jews who were there gathered around him, saying, "How long will you keep us in suspense? If you are the Messiah, tell us plainly." Jesus answered, ... I and the Father are one." (NIV)

A quick Google of "Jesus is God bible verse" turns up many other examples, some which require some understanding of Christian lingo such as "the Word" being Jesus. But overall it's fairly clear that the Bible claims Jesus is God.

So, if the Bible is true then Jesus _is_ God.

The following beliefs are relevant:
 - [[the_bible_is_a_trustworthy_source_of_jesus_public_teachings__belief]]
 - [[the_bible_is_a_trustworthy_source_of_all_jesus_teachings__belief]]
 - [[the_bible_is_a_trustworthy_source_of_all_jesus_teachings_and_a_bit_more__belief]]

Admittedly, this question is slightly different to each of those beliefs. But enough of the ideas are similar for those beliefs to be a good starting point.

Perhaps in the future I'll add more here specifically about the reliability of the Bible's claim of Jesus being God. <!--TODO add a bit here, once the points referenced above are written -->