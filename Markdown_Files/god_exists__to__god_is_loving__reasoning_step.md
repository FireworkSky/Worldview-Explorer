If an ultimate being exists then firstly, I really hope it is loving.


## An Ultimate being would be loving
I think it's hard to argue that a God existing implies that it would be loving. But it does seem plausible, because being loving is in some ways an ultimate/best thing which would in many ways need to be a property of an ultimate being.

------

But overall I think it's best to look at tangible candidates for a loving God and get information about how they have perhaps _really_ acted in this universe and outside of it to see if the God really was loving. And (jumping the gun a bit here) I think we find that is the case for the Christian God at least.