# Worldview Tree

_Welcome!_

[View the index](z-index.html)

[View the diagram](worldview_tree.svg)

Author: Daniel Britten

## Slack
Each page has _"Join the conversation on Slack"_ at the bottom taking you to the relevant channel, however you will first need to [join the Worldview Explorer Slack here](https://join.slack.com/t/worldview-explorer/shared_invite/zt-gk0pw09g-ypHCfVQRlegdTN95YY3QPQ).


----

[Join the conversation on Slack](https://worldview-explorer.slack.com/archives/C018R9TK4NQ)<!--This currently links to the general Slack channel-->