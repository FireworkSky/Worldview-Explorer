In response to the argument from evil my main thoughts are as follows. I will take the perspective of my own belief system as a Christian and try to highlight how the existence of evil doesn't 'break things'. Firstly though, I want to recognise that evil can be a devastating and tough battle and it affects each of us really profoundly and some of us have been _deeply_ hurt during our lives. This page is (currently) targeted more at our heads and how we think about evil rather than our hearts and how evil makes us feel but I want to acknowledge that both head and heart are really important.

Here are my key points:
 - God designed pain and suffering to only exist for a limited timespan, compared to an eternity of knowing God fully and being in loving community with Him. Because of this our present troubles are limited to a degree.
 - Moral free will (and thus real relationship with God) is not possible without the real possibility of beings that have the ability to cause real harm and pain (i.e. make choices in opposition to God).
 - God has stepped in as Jesus and can sympathise very really with even being tortured and put to death unjustly. He has experienced pain and suffering just like we have, and in most cases to a level at least as extreme as situations we are in (in human terms - in spiritual terms Jesus is the only human to have experienced complete separation from God the Father and the intense suffering that would have come with)
 - God promises to be with us and comfort us in our sufferings
 - God promises to one day wipe every tear from our eyes and bring about a situation where there is no more suffering.

These points give an overview of why I think evil existing (for a time) and God being real, powerful and loving can co-exist without giving rise to a contradiction.

It's worth pointing out here that I do not believe that any being ends up being eternally tortured. _That_ would be a contradiction I think, but time-limited evil, in light of the points above, is something I can sit with - especially knowing that God is right here with me through it all.