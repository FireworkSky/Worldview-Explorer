*Note: this was originally written in early 2021, with only minor improvements since then (so far).*

# Purpose
Generate something like: https://worldview-explorer.netlify.app/worldview_tree.svg

![Worldview Tree Example](./Haskell/worldview_tree.svg)

I.e. A directed graph with the brightness of the colours indicating the intensity of the belief. For filled nodes: green indicating agreement, red indicating disagreement, blue indicating uncertainty. The non-filled nodes: green indicating coherency between the indicated beliefs, red indicating dissonance between them. The white edges indicate "what it might take" for someone to move from believing the first belief to the second.

Each node and white edge is hyperlinked to a page with further long-form information about the belief. Each of these pages includes a link to a Slack channel where discussion about it could take place. (Note that [this](https://worldview-explorer.netlify.app/worldview_tree.svg) was more of an experiment and didn't actually attract any discussion on Slack).

Uses [dot](https://graphviz.org/doc/info/lang.html) as an intermediary step, e.g. the generated file [graph.gv](Haskell/graph.gv).

# Editing graph

The graph is generated from [Haskell/worldviewGraphData.hs](./Haskell/worldviewGraphData.hs). Edit it to replace the content of the graph's nodes and edges with your own data. The number after `Agree`/`Disagree`/`Unsure` indicates the strength of the belief/disbelief/uncertainty and effects the shade of the node. The value should be between 0 and 100.

# Building

## Dependencies
- [Haskell](https://www.haskell.org/), with the [Colour](https://wiki.haskell.org/Colour) and [Hashable](https://hackage.haskell.org/package/hashable) package.
- [GraphViz](https://graphviz.org/)

If you have [Nix](https://nixos.org) you can use:

`nix-shell -p graphviz "haskellPackages.ghcWithPackages (pkgs: [pkgs.colour pkgs.hashable])"`

Then:

`cd Haskell`

## For the graph (dot format)

```bash
ghc -O worldviewDatatypes.hs  worldviewGraphData.hs slackIdData.hs toDot.hs toBash.hs toSlack.hs toMarkdown.hs graphMain.hs
./graphMain > graph.gv
dot -Tsvg -o worldview_tree.svg graph.gv
```

For the hyperlinks to be correct all occurrences of "https://worldview-explorer.netlify.app/" should be appropriately replaced in toDot.hs.

## For the script to initialise all the Slack channels

If you skip this step, you may want to first edit line 32 of toMarkdown.hs so that it doesn't throw an error on a missing slackId.

```bash
ghc -O worldviewDatatypes.hs  worldviewGraphData.hs slackIdData.hs toDot.hs toBash.hs toSlack.hs toMarkdown.hs slackMain.hs
./slackMain > slack.sh
chmod +x slack.sh # Read this file for yourself before executing
export APITOKEN= #Put your Slack API token here (without any spaces)
./slash.sh > result.txt
# Manually examine result.txt and add lines to slackIdData.hs - You could use regexp, or a powerful text editor to transform result.txt into the appropriate format.
```

Don't forget re-do the above steps before re-running, including ghc compilation, after adding to slackIdData.hs and making other edits if you're adding new nodes/edges. The script only creates pages for items not in slackIdData.hs (And the data from that page is used for the URLs in the markdown output for linking to the appropriate Slack page).

It is meant to include a backlink to the markdown page, but this may be buggy. In any case line 21 of toSlack.hs would need to be updated to a different domain if needed.

## For appending links to the markdown pages

This adds a footer to existing markdown files for each node and white edge (which must follow a precise naming convention) to add a link to Slack.
e.g.
```markdown
----

[Join the conversation on Slack](https://worldview-explorer.slack.com/archives/C01AFBACRUG)

[Join the Slack workspace here if you haven't already](https://join.slack.com/t/worldview-explorer/shared_invite/zt-gk0pw09g-ypHCfVQRlegdTN95YY3QPQ)
```


```bash
ghc -O worldviewDatatypes.hs  worldviewGraphData.hs slackIdData.hs toDot.hs toBash.hs toSlack.hs toMarkdown.hs markdownMain.hs
cd ../Markdown_Files
mkdir -p Built
../Haskell/markdownMain
```

For publishing the markdown files there are many options. I used [neuron](https://neuron.zettel.page/), which is superseded by [emanote](https://emanote.srid.ca/).